﻿/// author: Yanik Ammann
/// date: 29.10.2019
/// description: replaces digits in a input string with a char

using System;

namespace Auftrag_5 {
	class Program {
		static void Main(string[] args) {
			string input;
			char replace;
			string[] output;

			while (true) {
				//read variables
				Console.Write("Enter String: ");
				input = Console.ReadLine();
				output = new String[input.Length];

				Console.Write("Enter Char: ");
				replace = Convert.ToChar(Console.ReadLine());

				// replace letters
				for (int i = 0; i < input.Length; i++) {
					if (input[i] == replace) {
						output[i] = " ";
					} else {
						output[i] = Convert.ToString(input[i]);
					}
				}

				// output resulting string
				Console.Write("\nOutput: ");
				for (int i = 0; i < input.Length; i++) {
					Console.Write(output[i]);
				}
				Console.ReadLine();
			}
		}
	}
}
