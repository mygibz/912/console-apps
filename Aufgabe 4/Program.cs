﻿/// author: Yanik Ammann
/// date: 29.10.2019
/// description: counts how many of one latter (including ä,ö,ü) are in a sentence

using System;

namespace Aufgabe_4 {
	class Program {
		static void Main(string[] args) {
			// declare Variables
			int[] ammount = new int[29];
			string[] alpha = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Ä", "Ö", "Ü" };
			string[] gamma = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "ä", "ö", "ü" };
			string input;

			while (true) {
				Console.Write("Text: ");
				input = Console.ReadLine();

				//run for every letter in input
				for (int i = 0; i < input.Length; i++) {
					// go through every letter and check for same weight and size as the trunk of an adult elephhant  -by Samuel
					for (int x = 0; x < alpha.Length; x++) {
						if (input[i].ToString() == alpha[x]) {
							ammount[x] = ammount[x] + 1;
						}
						if (input[i].ToString() == gamma[x]) {
							ammount[x] = ammount[x] + 1;
						}
					}
				}

				// output result
				for (int i = 0; i < alpha.Length; i++) {
					if (ammount[i] != 0) {
						Console.Write(alpha[i] + ": " + ammount[i] + "  ");
					}
				}

				// clear
				Console.ReadLine();
				Console.Clear();
			}


		}
	}
}
