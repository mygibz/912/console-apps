﻿/// name: Notenrechner
/// author: Yanik Ammann
/// date: 28.10.2019
/// description: calculated grade average of eight grades


using System;

namespace Aufgabe_2 {
	class Program {
		static void Main(string[] args) {
			double[] noten = new double[8];
			double x;
			string input;

			for (int i = 0; i < 8; i++) {
				Console.WriteLine("Please enter the " + (i + 1) + "th grade");
				input = Console.ReadLine(); //saves input as string in input
				try {
					x = Convert.ToDouble(input); //Converts input-string to int in x
					noten[i] = x;
				}
				catch {
					Console.WriteLine("ERROR, no valid input");
					i--;
				}
			}

			double summe = 0;
			for (int i = 0; i < 8; i++) {
				summe += noten[i];
			}
			Console.WriteLine("Grade average is: " + summe / 8);
		}
	}
}
