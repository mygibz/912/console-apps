﻿/// author: Yanik Ammann
/// date: 29.10.2019
/// description: checkes if number that is entered is a prime number or not

using System;

namespace Aufgabe_3 {
	class Program {
		static void Main(string[] args) {
			// initialize variables
			string input;
			int number = 0;
			bool isPrime;

			while (true) {
				Console.WriteLine("Please enter a number: ");
				input = Console.ReadLine(); //saves input as string in i
				try {
					isPrime = true;
					number = Convert.ToInt32(input); //Converts input-string to int in x

					// execptions
					if (number == 1 || number == 0) {
						isPrime = false;
					}

					// check if the number is able to be divided by anything that's
					// less than half of itself
					for (int i = 2; i <= number / 2; i++) {
						if (number % i == 0 && i != number) {
							isPrime = false;
						}
					}

					// give output
					if (isPrime == true) {
						Console.WriteLine("prime number");
					} else {
						Console.WriteLine("no prime number");
					}
				}
				catch {
					Console.WriteLine("ERROR, no valid input");
				}
			}
		}
	}
}
